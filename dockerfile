#FROM ghcr.io/graalvm/graalvm-ce:21.1.0
FROM oracle/graalvm-ce:21.1.0
WORKDIR /opt/graalvm
RUN gu install native-image
ENTRYPOINT ["native-image"]

# build the image
#docker build -t graalvm-native-image . 


# https://www.graalvm.org/docs/getting-started/container-images/
# https://blog.softwaremill.com/small-fast-docker-images-using-graalvms-native-image-99c0bc92e70b