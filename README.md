# Readme

- Install Graalvm 20.0.0.2
- install native image with `gu install native-image`
- initial spring project with Spring Native and Reactive Web
- create controller
- Add native image maven plugin
Follow this instruction to enable maven native image support
https://docs.spring.io/spring-native/docs/current/reference/htmlsingle/#getting-started-native-image

run `./mvnw -Pnative-image package`

For darwin, disable `--static` build.